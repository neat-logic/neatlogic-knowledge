[中文](README.md) / English



---

## About

neatlogic-knowledge is a knowledge base module.

## The main function

### Support multiple components

![img_1.png](README_IMAGES/img.png)
In addition to ordinary text, it also contains a variety of built-in components such as tables, pictures, lists, etc.,
which can easily realize various common layouts.

### Multiple versions

![img.png](README_IMAGES/img1.png)

- Support comparison between any versions.
  ![img.png](README_IMAGES/img3.png)

### Knowledge Audit.

- Support for embedding knowledge audits in ITSM processes.
  ![img.png](README_IMAGES/img4.png)

### Linkage with ITSM

- Supports direct conversion of completed work orders into knowledge base
  ![img.png](README_IMAGES/img2.png)